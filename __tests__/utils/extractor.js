const { extractValidFile, getBucketName } = require('../../src/utils/extractor')

describe('Extract file name and validate it', () => {
    test('With empty event', () => {
        const event = {}
        const file = extractValidFile(event)
        expect(file).toBe(undefined)
    })

    test('With empty Records array', () => {
        const event = {
            Records: [],
        }
        const file = extractValidFile(event)
        expect(file).toBe(undefined)
    })
    test('With records with empty object', () => {
        const event = {
            Records: [{}],
        }
        const file = extractValidFile(event)
        expect(file).toBe(undefined)
    })
    test('With empty s3', () => {
        const event = {
            Records: [
                {
                    s3: {},
                },
            ],
        }
        const file = extractValidFile(event)
        expect(file).toBe(undefined)
    })

    test('With empty s3 object', () => {
        const event = {
            Records: [
                {
                    s3: {
                        object: {},
                    },
                },
            ],
        }
        const file = extractValidFile(event)
        expect(file).toBe(undefined)
    })

    test('With empty s3 object key', () => {
        const event = {
            Records: [
                {
                    s3: {
                        object: {
                            key: '',
                        },
                    },
                },
            ],
        }
        const file = extractValidFile(event)
        expect(file).toBe(undefined)
    })

    test('With invalid file key', () => {
        const event = {
            Records: [
                {
                    s3: {
                        object: {
                            key: 'file.gif',
                        },
                    },
                },
            ],
        }
        const file = extractValidFile(event)
        expect(file).toBe(undefined)
    })

    test('With valid file png key', () => {
        const event = {
            Records: [
                {
                    s3: {
                        object: {
                            key: 'file.png',
                        },
                    },
                },
            ],
        }
        const file = extractValidFile(event)
        expect(file).toBe('file.png')
    })

    test('With valid file jpg key', () => {
        const event = {
            Records: [
                {
                    s3: {
                        object: {
                            key: 'file.jpg',
                        },
                    },
                },
            ],
        }
        const file = extractValidFile(event)
        expect(file).toBe('file.jpg')
    })

    test('With valid jpeg key', () => {
        const event = {
            Records: [
                {
                    s3: {
                        object: {
                            key: 'file.jpeg',
                        },
                    },
                },
            ],
        }
        const file = extractValidFile(event)
        expect(file).toBe('file.jpeg')
    })
})

describe('Extract bucket name', () => {
    test('With empty event', () => {
        const event = {}
        const file = getBucketName(event)
        expect(file).toBe(undefined)
    })

    test('With empty Records array', () => {
        const event = {
            Records: [],
        }
        const file = getBucketName(event)
        expect(file).toBe(undefined)
    })
    test('With records with empty object', () => {
        const event = {
            Records: [{}],
        }
        const file = getBucketName(event)
        expect(file).toBe(undefined)
    })
    test('With empty s3', () => {
        const event = {
            Records: [
                {
                    s3: {},
                },
            ],
        }
        const file = getBucketName(event)
        expect(file).toBe(undefined)
    })

    test('With empty s3 bucket', () => {
        const event = {
            Records: [
                {
                    s3: {
                        bucket: {},
                    },
                },
            ],
        }
        const file = getBucketName(event)
        expect(file).toBe(undefined)
    })

    test('With empty s3 bucket name', () => {
        const event = {
            Records: [
                {
                    s3: {
                        bucket: {
                            name: '',
                        },
                    },
                },
            ],
        }
        const file = getBucketName(event)
        expect(file).toBe(undefined)
    })

    test('With empty s3 bucket name', () => {
        const event = {
            Records: [
                {
                    s3: {
                        bucket: {
                            name: 'mybucket',
                        },
                    },
                },
            ],
        }
        const file = getBucketName(event)
        expect(file).toBe('mybucket')
    })
})
