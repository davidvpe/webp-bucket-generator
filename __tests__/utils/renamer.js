const { nameWithWebp } = require('../../src/utils/renamer')

describe('Checking renamings', () => {
    test('with double format', () => {
        const oldName = 'hello.jpg.png'
        const newName = nameWithWebp(oldName)
        expect(newName).toBe('hello.jpg.webp')
    })

    test('with one format', () => {
        const oldName = 'hello.jpg'
        const newName = nameWithWebp(oldName)
        expect(newName).toBe('hello.webp')
    })

    test('with webp already', () => {
        const oldName = 'hello.webp'
        const newName = nameWithWebp(oldName)
        expect(newName).toBe('hello.webp')
    })

    test('with no format', () => {
        const oldName = 'hello'
        const newName = nameWithWebp(oldName)
        expect(newName).toBe('hello')
    })
})
