const { convertFile } = require('./utils/converter')
const { extractValidFile, getBucketName } = require('./utils/extractor')
const { downloadFile, uploadFile } = require('./utils/s3')
const { nameWithWebp } = require('./utils/renamer')

module.exports.webpadd = async (event, context) => {
    const file = extractValidFile(event)
    if (!file) {
        console.log('not valid file')
        return
    }
    console.log('extracted', file)
    const bucketName = getBucketName(event)
    console.log('got bucket name', bucketName)
    const fileBuffer = await downloadFile(file, bucketName)
    console.log('downloaded file', fileBuffer.byteLength)
    const webpBuffer = await convertFile(file, fileBuffer, 70)
    console.log('converted file', webpBuffer.byteLength)
    const webpFileName = nameWithWebp(file)
    console.log('generated new name', webpFileName)
    await uploadFile(webpBuffer, webpFileName, bucketName)
    console.log('uploaded new file', webpFileName)
    return
}
