//@ts-check

const { S3 } = require('aws-sdk')

const s3 = new S3()

const downloadFile = (/** @type {string} */ fileName, /** @type {string} */ bucketName) => {
    return s3
        .getObject({
            Bucket: bucketName,
            Key: fileName,
        })
        .promise()
        .then((output) => output.Body)
}

const uploadFile = (/** @type {string} */ buffer, /** @type {string} */ key, /** @type {string} */ bucketName) => {
    return s3
        .putObject({
            Key: key,
            Bucket: bucketName,
            Body: buffer,
        })
        .promise()
}

module.exports = {
    downloadFile,
    uploadFile,
}
