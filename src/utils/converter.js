//@ts-check

const path = require('path')
const webp = require('webp-converter')

module.exports.convertFile = async (/** @type {string} */ fileName, /** @type {Buffer} */ fileBuff, /** @type {number} */ quality) => {
    const extension = path.extname(fileName).slice(1)
    return webp.buffer2webpbuffer(fileBuff, extension, `-q ${quality}`, '/tmp/')
}
