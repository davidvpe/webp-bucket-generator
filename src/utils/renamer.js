//@ts-check
const path = require('path')

module.exports.nameWithWebp = (/** @type {string} */ fileName) => {
    const extension = path.extname(fileName)
    if (!extension || extension.length === 0) return fileName
    const index = fileName.lastIndexOf(extension)
    return fileName.slice(0, index) + '.webp'
}
