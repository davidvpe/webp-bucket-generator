const extractS3 = (event) => {
    const records = event['Records']
    if (!records || records.length === 0) return undefined
    const record = records[0]
    const s3 = record.s3
    if (!s3) return undefined
    return s3
}

module.exports.extractValidFile = (event) => {
    const s3 = extractS3(event)
    if (!s3) return undefined
    const obj = s3.object
    if (!obj) return undefined
    const fileName = obj.key
    if (!fileName) return undefined

    const validFormats = ['png', 'jpg', 'jpeg']

    if (validFormats.some((suff) => fileName.endsWith(`.${suff}`))) {
        return fileName
    } else {
        return undefined
    }
}

module.exports.getBucketName = (event) => {
    const s3 = extractS3(event)
    if (!s3) return undefined
    const bucket = s3.bucket
    if (!bucket) return undefined
    const bucketName = bucket.name
    if (!bucketName) return undefined
    return bucketName
}
