# WebP Bucket Generator

Hi! This is my first project using **AWS Lambda** and **Serverless Framework**

With this project you can deploy a S3 Bucket and every png, jpg, jpeg file that gets dropped will be automatically converted to **webp** and hosted along the original file. The idea behind it is that you are able to use them both on a website to take advantage of this (as Google says) _next generation_ format

## What to do?

This project is ready to rock and roll it just needs a couple of environment variables

    * BUCKET_NAME=thisismybucket  //Bucket_name you want to use or create
    * BUCKET_EXISTS=false  //Bucket already exists or you want to create a new one?

Of course this also requires you to have your AWS credentials setup so that the Serverless Framework can do its magic.
